else if(message.type == messageType.newChatMember) {
	String uname = message.getUsername();
	long uid = message.getUserId();
	if(uname.equals("@VillageWWinfoBot")){
		message.answerMessage(new TextMessage(String.format("Hi, ich bin %s und ich kann euch helfen das Spiel \"Werwolf\" zu verstehen! Ich bin der private Bot der Gruppe @werwolfundsoo.", uname)));

	} else if(chat_id == Main.suuportGrpId) {
		//Support Group join message
		String help = String.format("Willkommen %s (Userid: %d) in der Supportgruppe. Alle Supporter wurden über dein Hilfeersuchen benachrichtigt.", uname, uid);
		message.answerMessage(new TextMessage());
		long[] supportids = TgMethods.getChatAdministrators(chat_id);
		TextMessage notifymsg = new TextMessage(String.format("Hallo, %s (Userid: %d) ist der Supportgruppe gejoint und möchte vermutlich Hilfe haben. Du bekommst die Nachricht, weil du in der Support Gruppe Admin bist.", uname, uid));
		for(int i = 0; i < supportids.length; i++){
			if(supportids[i] != 338292902){
				notifymsg.setChatId(supportids[i]);
				notifymsg.send();  
			}
		}
	}
	else message.answerMessage(new TextMessage(String.format("Willkommen %s (Userid: %d) in der Gruppe! Für eine Erklärung zum Spiel, einer Liste der Rollen und der Gruppenregeln klicke bitte..", uname, uid)).addInlineButton("..Hier!", "https://t.me/VillageWWinfoBot", Message.inlineButtonType.url));
}
