import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import Exceptions.DuplicateItemException;
import Exceptions.NoSuchIDException;

public class ScieboWatcher implements Runnable {

	// Save Files
	private File save_files;
	private File save_subscribers;

	// Where to find the files
	private File sciebo_dir;

	// Lists
	private ArrayList<File> files;
	private ArrayList<Long> subscriptions;

	// Rest
	// der MatseBot, welcher die notifications versendet
	private MatseTelegramBot bot;
	// der thread, welcher die notifications versendet
	private Thread thread;

	public ScieboWatcher(MatseTelegramBot bot){
		this.subscriptions = new ArrayList<>();
		this.sciebo_dir = new File("cloud/Hausaufgaben/Semester 2");
		this.files = new ArrayList<>();
		this.save_files = new File("sciebo_files.txt");
		this.save_subscribers = new File("sciebo_subscribers.txt");
		this.bot = bot;
		this.init();
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * Create all notifications and add them to the matse bot
	 */
	public void run(){
		while(true){
			System.out.println("Scanne nach neuen Dateien");
			ArrayList<SendMessage> allSubscribersNotifications = this.getAllSubscribersNotifications();
			for(SendMessage subscriberNotification: allSubscribersNotifications){
				this.bot.addMessageRequest(subscriberNotification);
			}
			try{
				Thread.sleep(4000);
			} catch(InterruptedException ie){
				return;
			}
		}
	}

	/**
	 * Loads add nessesary data from the save files and initialises them.
	 */
	public void init(){
		this.initFiles();
		this.initSubscribers();
		//getAllSubscribersNotifications (enthaelt die updates) muss nicht ausgefuehrt werden, da es eh automatisch später gemacht wird
	}

	/**
	 * Loads add ScieboFiles from a save file and adds them to the files list.
	 * This is, to know which files are beeing watched.
	 */
	public void initFiles(){
		//load files from save file
		Scanner sc;
		try {
			sc = new Scanner(this.save_files);
		} catch(FileNotFoundException fnfe){
			//es gibt keine datei, was nicht schlimm ist
			return;
		}
		//add files to this.files
		while(sc.hasNextLine()){
			String s = sc.nextLine();
			if(!s.equals("")){
				File f = new File(s);
				System.out.println("Initialized file: "+f.getAbsoluteFile());
				this.files.add(f);
			}
		}
		sc.close();
	}

/**
 * Loads all Subscribers from a save file and adds them to the subscribers list.
 */
public void initSubscribers(){
	Scanner sc;
	try {
		sc = new Scanner(this.save_subscribers);
	} catch(FileNotFoundException fnfe){
		return;
	}
	while(sc.hasNextLine()){
		String s = sc.nextLine();
		if(!s.equals("")){
			long d = Long.parseLong(s);
			System.out.println("Initialized subscriber: "+d);
			this.subscriptions.add(d);
		}
	}
	sc.close();
}

/**
 * Saves all data to files, to be able to restore everything on the
 * next startup.
 */
public void save(){
	this.saveFiles();
	this.saveSubscribers();
}

/**
 * Saves all sciebo files to a save file.
 */
public void saveFiles(){
	String save = "";
	for(File f: this.files){
		save+=f.getPath()+"\n";
	}
	FileWriter fw;
	try {
		fw = new FileWriter(this.save_files);
		fw.write(save);
		fw.close();
	} catch(IOException ioe){
		ioe.printStackTrace();
		System.exit(1);
	}
}

/**
 * Saves all subscribers to a save file.
 */
public void saveSubscribers(){
	String save = "";
	for(long l: this.subscriptions){
		save+=l+"\n";
	}
	FileWriter fw;
	try {
		fw = new FileWriter(this.save_subscribers);
		fw.write(save);
		fw.close();
	} catch(IOException ioe){
		ioe.printStackTrace();
		System.exit(1);
	}
}

/**
 * Fuegt eine Telegram chat id zu der Liste der Subscriber hinzu.
 */
public String addSubscriber(long id){
	for(Long subscriber: this.subscriptions){
		if(subscriber.equals(id)){
			throw new DuplicateItemException("Du hast den Dienst bereits aktiviert");
		}
	}
	this.subscriptions.add(id);
	this.saveSubscribers();
	System.out.println("New subscriber added.");
	return "Der Dienst ist nun aktiv. Du wirst nun fortlaufend benachrichtigt, sobald eine neue Datei im Sciebo erscheint.\nDeaktivieren mit /cancel_subscription.";
}

/**
 * Entfernt eine Telegram chat id von der Liste der Subscriber
 */
public String removeSubscriber(long id){
	//TODO: void, anstatt einen String zu returnen und einen
	//Fehler werfen, falls es nicht funktioniert hat
	for(int i= 0; i<this.subscriptions.size(); i++){
		Long subscriber = this.subscriptions.get(i);
		if(subscriber.equals(id)){
			this.subscriptions.remove(i);
			return "Der Dienst wurde deaktivert. Du erhältst nun keine Benachrichtigungen mehr, wenn neue Dateien im Sciebo erscheinen.";
		}
	}
	this.saveSubscribers();
	throw new NoSuchIDException("Du hast den Dienst noch nicht aktiviert, also kannst du ihn auch nicht deaktivieren ;).");
}

/**
 * Gibt eine Liste mit SendMessages zurueck. Diese SendMessages enthalten die Nachrichten an alle Subscriber mit den neuen Dateien
 */
public ArrayList<SendMessage> getAllSubscribersNotifications(){
	ArrayList<SendMessage> out = new ArrayList<>();
	String new_files = this.getNewFiles();
	if(new_files!=null){
		for(Long subscriber: this.subscriptions){
			//TODO
			SendMessage sendMessageRequest = new SendMessage();
			sendMessageRequest.setChatId((long) subscriber);
			sendMessageRequest.setText(new_files);
			out.add(sendMessageRequest);
		}
	}
	return out;
}

/**
 * Gibt eine Auflistung von neuen Dateien als String zurueck.
 * Falls es keine neuen Dateien gibt, wird null zurueckgegeben.
 */
public String getNewFiles(){
	//TODO remove removed Files and inform users
	FileUtils fileUtils = new FileUtils();
	Collection<File> all_files = fileUtils.listFiles(this.sciebo_dir,TrueFileFilter.INSTANCE,TrueFileFilter.INSTANCE);
	// get new files
	ArrayList<File> new_files = new ArrayList<>();
	for(File f: all_files){
		boolean new_file = true;
		for(File local: this.files){
			if(local.equals(f)){
				new_file=false;
			}
		}
		if(new_file){
			this.files.add(f);
			new_files.add(f);
		}
	}
	//generate output string
	String out = null;
	if(new_files.size()>0){
		out = "Sciebo Updates:\nNew Files Added:\n";
		for(File new_f: new_files){
			out+="\n- "+new_f.getName();
		}
		this.saveFiles();
	}
	return out;
}

public String listAllFiles(){
	//TODO eine ordnetliche ordner strktur ausgeben
	String out = "Keine Dateien vorhanden.";
	if(this.files.size()>0){
		out = "Dateien:\n\n";
		for(File f: this.files){
			out+=f.getParent()+": \n";
			out+=f.getName()+"\n";
		}
	}
	return out;
}
}
