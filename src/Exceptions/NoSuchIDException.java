package Exceptions;

public class NoSuchIDException extends RuntimeException {
	public NoSuchIDException(){
		super();
	}
	public NoSuchIDException(String s){
		super(s);
	}
}
