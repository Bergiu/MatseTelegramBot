package Exceptions;

public class DuplicateItemException extends RuntimeException {

	public DuplicateItemException(String s){
		super(s);
	}

	public DuplicateItemException(){
		super();
	}
}
