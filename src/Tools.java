import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {

	/**
	 * returns a string that is contained in the regex group of the command.
	 *
	 * @param command
	 * @param regex
	 * @param group
	 * @return
	 */
	public static String getRegExGroup(String command, String regex, int group) {
		Pattern pattern = Pattern.compile(regex.toLowerCase());
		Matcher matcher = pattern.matcher(command.toLowerCase());
		if (matcher.matches()) {
			if (matcher.groupCount() >= group) {
				String s = matcher.group(group);
				return s;
			}
		}
		System.out.println("warning, no match");
		return null;
	}

	/**
	 * Iterates through an Array of Regex Strings. If the command matches one of these
	 * Regexes, the method returns the requested Regex group from the command
	 */
	public static String getRegExGroupArray(String command, String[] regexs, int group) {
		for(String regex: regexs){
			if(Tools.lowerCaseMatches(command, regex)) {
				return Tools.getRegExGroup(command, regex, 1);
			}
		}
		return null;
	}

	/**
	 * The method changes (locally) the command and the regex to LowerCases. If they match it
	 * returns true.
	 */
	public static boolean lowerCaseMatches(String command, String regex) {
		if (command.toLowerCase().matches(regex.toLowerCase())) {
			return true;
		}
		return false;
	}
	
	/**
	 * The command iterates through an array of regexs. Then it changes (locally) the
	 * command and the regex to LowerCase. If one of these regexs matches the command
	 * it returns true.
	 */
	public static boolean lowerCaseMatchesArray(String command, String[] regexs){
		for(String regex: regexs){
			if(lowerCaseMatches(command, regex)){
				return true;
			}
		}
		return false;
	}

}
