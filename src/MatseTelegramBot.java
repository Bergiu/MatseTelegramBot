import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

import Exceptions.DuplicateItemException;

public class MatseTelegramBot extends TelegramLongPollingBot implements Runnable {

	private String botUsername;

	private String botToken;

	private ScieboWatcher scieboWatcher;

	// Thread save message queue
	private ConcurrentLinkedQueue<SendMessage> messageQueue;

	// Thread that sends the messages
	private Thread sendMessagesThread;

	public MatseTelegramBot(){
		String[] params = readTokenAndUsernameFromFile();
		this.botToken = params[0];
		this.botUsername = params[1];
		this.messageQueue = new ConcurrentLinkedQueue<SendMessage>();
		this.scieboWatcher = new ScieboWatcher(this);
		this.sendMessagesThread = new Thread(this);
		this.sendMessagesThread.start();
	}

	public static String[] readTokenAndUsernameFromFile() {
		File file_token = new File("TOKEN");
		File file_username = new File("BOTNAME");
		try {
			Scanner sc = new Scanner(file_token);
			String token = sc.nextLine();
			sc.close();
			sc = new Scanner(file_username);
			String username = sc.nextLine();
			sc.close();
			if (token != "" && username != "") {
				return new String[] { token, username };
			}
		} catch (FileNotFoundException fnfe) {

		}
		System.out.println("You have to create two files, TOKEN and BOTNAME.");
		System.out.println("Then you have to insert your BotToken and your BotUsername into them.");
		System.exit(1);
		return null;
	}

	@Override
	public String getBotUsername() {
		return this.botUsername;
	}

	@Override
	public String getBotToken() {
		return this.botToken;
	}

	@Override
	public void onUpdateReceived(Update update) {
		// TODO Auto-generated method stub
		if (update.hasMessage()) {
			Message message = update.getMessage();
			if (message.hasText()) {
				// get required params
				int tg_id = message.getFrom().getId();
				long chat_id = message.getChat().getId();
				String text = this.removeBotUsername(message.getText());
				// some console logs
				//TODO log each message with chat and member id (and name)
				System.out.println(tg_id+" writes:");
				System.out.println(text);
				// the message to send
				SendMessage sendMessageRequest = null;
				// if new member joins
				if(message.getNewChatMember() != null){
					//TODO implement
					System.out.println("Ein neuer User ist der Gruppe beigetreten");
				}
				switch(text){
					case "/subscribe_sciebo":
						sendMessageRequest = this.add_sciebo_subscription(chat_id);
						break;
					case "/list_files":
						sendMessageRequest = this.listFiles(chat_id);
						break;
						// case "/auto_send_new_files":
						// 	this.autoSendNewFiles(chat_id);
					default:
						sendMessageRequest = this.default_msg(chat_id);
				}
				this.addMessageRequest(sendMessageRequest);
			}
		}
	}

	/**
	 * Sends all notifications
	 */
	public void run(){
		while(true){
			System.out.println("Bearbeite messageQueue. Unsend messages: "+this.messageQueue.size());
			// erzeuge eine kopie der liste
			ConcurrentLinkedQueue<SendMessage> messages = new ConcurrentLinkedQueue<>();
			for(SendMessage message: this.messageQueue){
				messages.add(message);
			}
			// bearbeite die kopierte liste und entfernen aus der originalen
			for(SendMessage message: messages){
				if(message!=null){
					try {
						sendMessage(message);
						this.messageQueue.remove(message);
					} catch (TelegramApiException e) {
						BotLogger.error("LOGTAG", e);
						// do some error handling
					}
				}
			}
			try{
				Thread.sleep(1000);
			} catch(InterruptedException ie){
				return;
			}
		}
	}

	/**
	 * Generates a SendMessage to chat_id with the given message
	 */
	public static SendMessage generateMessage(long chat_id, String message){
		SendMessage sendMessageRequest = new SendMessage();
		sendMessageRequest.setChatId(chat_id+"");
		sendMessageRequest.setText(message);
		return sendMessageRequest;
	}

	/**
	 * This is the default method, that gets executed, if no message matches
	 */
	public SendMessage default_msg(Message message){
		SendMessage sendMessageRequest = new SendMessage();
		sendMessageRequest.setChatId(message.getChatId().toString());
		sendMessageRequest.setText("This bot is under development!");
		return sendMessageRequest;
	}

	/**
	 * Adds a message to the message queue
	 */
	public void addMessageRequest(SendMessage sendMessageRequest){
		if(sendMessageRequest!=null){
			this.messageQueue.add(sendMessageRequest);
		}
	}

	/**
	 * Adds the chat id to the list of sciebo subscribers
	 */
	public SendMessage add_sciebo_subscription(long chat_id){
		String message;
		try {
			message = this.scieboWatcher.addSubscriber(chat_id);
		} catch(DuplicateItemException die){
			message = die.getMessage();
		}
		return MatseTelegramBot.generateMessage(chat_id,message);
	}

	/**
	 * lists all curently recognised files from the sciebo watcher
	 */
	public SendMessage listFiles(long chat_id){
		String all_files = this.scieboWatcher.listAllFiles();
		return MatseTelegramBot.generateMessage(chat_id,all_files);
	}

	/**
	 * the deault message, if a user types a wrong command
	 */
	public SendMessage default_msg(long chat_id){
		String message = "";
		message+= "This bot is under development!";
		message+= "\n\nAvailable Commands:\n";
		message+= "/list_files";
		message+= "/activate";
		return MatseTelegramBot.generateMessage(chat_id, message);
	}

	/**
	 * returns the message without the @botusername at the end
	 */
	private String removeBotUsername(String message) {
		if (Tools.lowerCaseMatches(message, "(.*)" + this.getBotUsername())) {
			message = Tools.getRegExGroup(message, "(.*)" + this.getBotUsername(), 1);
		}
		return message;
	}

}
